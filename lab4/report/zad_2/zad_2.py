import numpy as np
import cv2
import matplotlib.pyplot as plt

def gaussian_noise(img, mu, sigma):
  out = img.astype(np.float32)
  noise = np.random.normal(mu, sigma, img.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.uint8)
def salt_n_pepper_noise(img, percent=10):
  out = img.astype(np.float32)
  limit = ((float(percent)/2.0)/100.0) * 255.0
  noise = np.random.uniform(0,255, img.shape)
  out[noise<limit] = 0
  out[noise>(255-limit)] = 255
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)
def uniform_noise(img, low, high):
  out = img.astype(np.float32)
  noise = np.random.uniform(low,high, img.shape)
  out = out + noise
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)
def show(img):
  cv2.imshow("title", img)
  cv2.waitKey(0)
  cv2.destroyAllWindows()
def showhist(img):
  hist,bins = np.histogram(img.flatten(), bins=256, range=(0,255))
  plt.vlines(np.arange(len(hist)), 0, hist)
  plt.title("Histogram")
  plt.show()

img = cv2.imread("zad2_1_corruptedimages/gauss_5_boats.jpg",0)

cv2.imwrite('gauss_5_boats_median.jpg', cv2.medianBlur(img, 3))
sigma=1
cv2.imwrite('gauss_5_boats_gauss.jpg', cv2.GaussianBlur( img, (5,5), sigma))


#mu=0
#sigma=35 #5 15 35
#cv2.imwrite('gauss_35_airplane.jpg', gaussian_noise(img, mu, sigma))
#cv2.imwrite('gauss_35_boats.jpg',gaussian_noise(img2, mu, sigma))

#percent=10 #1 10
#cv2.imwrite('saltpepper_10_airplane.jpg',salt_n_pepper_noise(img,percent))
#cv2.imwrite('saltpepper_10_boats.jpg',salt_n_pepper_noise(img2,percent))

#l=-60
#h=60
#show(uniform_noise(img, l, h))
#showhist(uniform_noise(img, l, h))