import numpy as np
import cv2
import matplotlib.pyplot as plt

def gaussian_noise(img, mu, sigma):
  out = img.astype(np.float32)
  noise = np.random.normal(mu, sigma, img.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.uint8)

def salt_n_pepper_noise(img, percent=10):
  out = img.astype(np.float32)
  limit = ((float(percent)/2.0)/100.0) * 255.0
  noise = np.random.uniform(0,255, img.shape)
  out[noise<limit] = 0
  out[noise>(255-limit)] = 255
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)

def uniform_noise(img, low, high):
  out = img.astype(np.float32)
  noise = np.random.uniform(low,high, img.shape)
  out = out + noise
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)

def show(img):
  cv2.imshow("title", img)
  cv2.waitKey(0)
  cv2.destroyAllWindows()
  
def showhist(img):
  hist,bins = np.histogram(img.flatten(), bins=256, range=(0,255))
  plt.vlines(np.arange(len(hist)), 0, hist)
  plt.title("Histogram")
  plt.show()

img = cv2.imread("../../images/lenna.bmp",0)

l=-60
h=60
show(uniform_noise(img, l, h))
cv2.imwrite('uniform_60.jpg',uniform_noise(img, l, h))
showhist(uniform_noise(img, l, h))

#mu=0
#sigma=60
#show(gaussian_noise(img, mu, sigma))
#cv2.imwrite("gaussian_sigma60.jpg", gaussian_noise(img, mu, sigma))
#showhist(gaussian_noise(img, mu, sigma))

#percent=20
#show(salt_n_pepper_noise(img,percent))
#cv2.imwrite("salt_n_pepper_20percent.jpg",salt_n_pepper_noise(img,percent))
#showhist(salt_n_pepper_noise(img,percent))

