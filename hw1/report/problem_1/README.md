# Problem_1 report
## Author: Abraham Kostic

This program adds horizontal and vertical stripes on every image in folder images.

The 'stripes_v' function returns numpy array that has its every other
column set to 0, so every other pixel vertically is black.
The 'stripes_h' function returns numpy array that has its every other
row set to 0, so every other pixel horizontally is black.

### Horizontal stripes

![](airplane_H.png)

![](asbest_H.png)

![](baboon_H.png)

![](barbara_H.png)

![](boats_H.png)

![](BoatsColor_H.png)

![](goldhill_H.png)

![](lenna_H.png)

![](noise_template_H.png)

![](otsu_H.png)

![](pepper_H.png)

![](sudoku_H.png)


### Vertical stripes

![](airplane_V.png)

![](asbest_V.png)

![](baboon_V.png)

![](barbara_V.png)

![](boats_V.png)

![](BoatsColor_V.png)

![](goldhill_V.png)

![](lenna_V.png)

![](noise_template_V.png)

![](otsu_V.png)

![](pepper_V.png)

![](sudoku_V.png)


