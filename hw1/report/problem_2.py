import cv2
import numpy as np
import os

PROJECT_PATH = "C:\\Users\\Abri\\osirv_labs"

def load_images(images_path, grayscale=1):
    """ Outputs: list of loaded images as numpy arrays and list of image filenames"""
    images = []
    image_names = []
    # os.listdir returns a list of filenames in a directory passed as parameter
    for image in os.listdir(images_path):
        image_path = os.path.join(images_path, image)
        if grayscale:
            open_as_flag = cv2.IMREAD_GRAYSCALE
        else:
            open_as_flag = cv2.IMREAD_COLOR
        #print(image)
        images.append(cv2.imread(image_path, open_as_flag))
        image_names.append(image)
    return images, image_names

def show(image):
    # Break if trying to show image that is not uint8 datatype. You should always
    # convert your images to np.uint8 before trying to display them
    assert image.dtype == np.uint8, "Image is not np.uint8 datatype, convert to appropriate datatype"
    cv2.imshow("Image", image)
    cv2.waitKey()
    cv2.destroyAllWindows()

def save_image(image, image_name, append_string=""):
    # Split the string on '.' character and take all splits except the last one 
    # The last split should be the extension. This implementation works with files 
    # which have variable extension size (e.g. jpg and jpeg), but requires that file 
    # has an extension. Creates a list of strings from the input string, split on places
    # where '.' character appears
    base_name = image_name.split('.')[:-1]
    # Joins the list of strings from previous splitting
    base_name = "".join(base_name)
    #print ("Base name for " + image_name + " is " + base_name)
    # Create absolute path to the project
    ######### Change this for your problem, e.g. os.path.join(PROJECT_PATH, "hw1", "problem_1")
    base_path = os.path.join(PROJECT_PATH, "hw1", "report", "problem_2")
    save_path = os.path.join(base_path, base_name + append_string + ".png")
    #print ("Saving the image to: " + save_path)
    cv2.imwrite(save_path, image)

images_path = os.path.join(PROJECT_PATH, "images")
images, image_names = load_images(images_path, grayscale=0)

def fat_h(image, width):
    h_len = image.shape[1]//width
    if len(image.shape) == 2:
        for x in range (0, h_len, 2):
            image[x*width:((x+1)*width),:] = 0
    elif len(image.shape) == 3:
        for x in range (0, h_len, 2):
            image[x*width:((x+1)*width),:,:] = 0
    return image.astype(np.uint8)

def fat_v(image, width):
    v_len = image.shape[1]//width
    if len(image.shape) == 2:
        for y in range (0, v_len, 2):
            image[:, y*width:((y+1)*width)] = 0
    elif len(image.shape) == 3:
        for y in range (0, v_len, 2):
            image[:, y*width:((y+1)*width),:] = 0
    return image.astype(np.uint8)

for i, image in enumerate(images):
    for width in [8, 16, 32]:
        h = fat_h(image.copy(),width)
        v = fat_v(image.copy(),width)
        #show(h)
        #show(v)
        save_image(h, image_names[i], "_H"+ str(width))
        save_image(v, image_names[i], "_V"+ str(width))



