import cv2
import numpy as np
import os

PROJECT_PATH = "C:\\Users\\Abri\\osirv_labs"

def load_images(images_path, grayscale=1):
    """ Outputs: list of loaded images as numpy arrays and list of image filenames"""
    images = []
    image_names = []
    # os.listdir returns a list of filenames in a directory passed as parameter
    for image in os.listdir(images_path):
        image_path = os.path.join(images_path, image)
        if grayscale:
            open_as_flag = cv2.IMREAD_GRAYSCALE
        else:
            open_as_flag = cv2.IMREAD_COLOR
        #print(image)
        images.append(cv2.imread(image_path, open_as_flag))
        image_names.append(image)
    return images, image_names

def show(image):
    # Break if trying to show image that is not uint8 datatype. You should always
    # convert your images to np.uint8 before trying to display them
    assert image.dtype == np.uint8, "Image is not np.uint8 datatype, convert to appropriate datatype"
    cv2.imshow("Image", image)
    cv2.waitKey()
    cv2.destroyAllWindows()

def save_image(image, image_name, append_string=""):
    # Split the string on '.' character and take all splits except the last one 
    # The last split should be the extension. This implementation works with files 
    # which have variable extension size (e.g. jpg and jpeg), but requires that file 
    # has an extension. Creates a list of strings from the input string, split on places
    # where '.' character appears
    base_name = image_name.split('.')[:-1]
    # Joins the list of strings from previous splitting
    base_name = "".join(base_name)
    #print ("Base name for " + image_name + " is " + base_name)
    # Create absolute path to the project
    ######### Change this for your problem, e.g. os.path.join(PROJECT_PATH, "hw1", "problem_1")
    base_path = os.path.join(PROJECT_PATH, "hw1", "report", "problem_1")
    save_path = os.path.join(base_path, base_name + append_string + ".png")
    #print ("Saving the image to: " + save_path)
    cv2.imwrite(save_path, image)

images_path = os.path.join(PROJECT_PATH, "images")
images, image_names = load_images(images_path, grayscale=0)

def stripes_h(imageh):
    if len(imageh.shape) == 2:
        imageh[::2,::] = 0
    elif len(imageh.shape) == 3:
        imageh[::2,::,:] = 0
    return imageh.astype(np.uint8)

def stripes_v(imagev):
    if len(imagev.shape) == 2:
        imagev[::,::2] = 0
    elif len(imagev.shape) == 3:
        imagev[::,::2,:] = 0
    return imagev.astype(np.uint8)

for i, image in enumerate (images):
    v = stripes_v(image.copy())
    h = stripes_h(image.copy())
    #show(vertical)
    #show(horizontal)
    save_image(v, image_names[i], "_V")
    save_image(h, image_names[i], "_H")

