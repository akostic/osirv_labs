# Problem_4 report
## Author: Abraham Kostic

This program adds checkerboard pattern on every image with width of squares 32 pixel.
Instead of white and black squares, every other square has its colors inverted, and the
rest remain the same


### Checkered images

![](airplane_checkinv.png)

![](asbest_checkinv.png)

![](baboon_checkinv.png)

![](barbara_checkinv.png)

![](boats_checkinv.png)

![](BoatsColor_checkinv.png)

![](goldhill_checkinv.png)

![](lenna_checkinv.png)

![](noise_template_checkinv.png)

![](otsu_checkinv.png)

![](pepper_checkinv.png)

![](sudoku_checkinv.png)

