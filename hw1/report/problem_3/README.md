# Problem_3 report
## Author: Abraham Kostic

This program adds checkerboard pattern on every image with width of squares 32 pixel.
Instead of white squares, original pixel data remain, only every other square is changed.


### Checkered images

![](airplane_checkered.png)

![](asbest_checkered.png)

![](baboon_checkered.png)

![](barbara_checkered.png)

![](boats_checkered.png)

![](BoatsColor_checkered.png)

![](goldhill_checkered.png)

![](lenna_checkered.png)

![](noise_template_checkered.png)

![](otsu_checkered.png)

![](pepper_checkered.png)

![](sudoku_checkered.png)

