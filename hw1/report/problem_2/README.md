# Problem_2 report
## Author: Abraham Kostic

This program adds horizontal and vertical stripes on every image in folder images,
but every stripe is 8, 16 and 32 pixel wide.

The 'fat_h' is function that accepts image and width of stripe as arguments, 
and then returns image (numpy array) that has horizontal black stripes wide 
as second argument.
The 'fat_v' is function that accepts image and width of stripe as arguments, 
and then returns image (numpy array) that has vertical black stripes wide 
as second argument.

### Horizontal stripes - width 8

![](airplane_H8.png)

![](asbest_H8.png)

![](baboon_H8.png)

![](barbara_H8.png)

![](boats_H8.png)

![](BoatsColor_H8.png)

![](goldhill_H8.png)

![](lenna_H8.png)

![](noise_template_H8.png)

![](otsu_H8.png)

![](pepper_H8.png)

![](sudoku_H8.png)


### Vertical stripes - width 8

![](airplane_V8.png)

![](asbest_V8.png)

![](baboon_V8.png)

![](barbara_V8.png)

![](boats_V8.png)

![](BoatsColor_V8.png)

![](goldhill_V8.png)

![](lenna_V8.png)

![](noise_template_V8.png)

![](otsu_V8.png)

![](pepper_V8.png)

![](sudoku_V8.png)




### Horizontal stripes - width 16

![](airplane_H16.png)

![](asbest_H16.png)

![](baboon_H16.png)

![](barbara_H16.png)

![](boats_H16.png)

![](BoatsColor_H16.png)

![](goldhill_H16.png)

![](lenna_H16.png)

![](noise_template_H16.png)

![](otsu_H16.png)

![](pepper_H16.png)

![](sudoku_H16.png)


### Vertical stripes - width 16

![](airplane_V16.png)

![](asbest_V16.png)

![](baboon_V16.png)

![](barbara_V16.png)

![](boats_V16.png)

![](BoatsColor_V16.png)

![](goldhill_V16.png)

![](lenna_V16.png)

![](noise_template_V16.png)

![](otsu_V16.png)

![](pepper_V16.png)

![](sudoku_V16.png)




### Horizontal stripes - width 32

![](airplane_H32.png)

![](asbest_H32.png)

![](baboon_H32.png)

![](barbara_H32.png)

![](boats_H32.png)

![](BoatsColor_H32.png)

![](goldhill_H32.png)

![](lenna_H32.png)

![](noise_template_H32.png)

![](otsu_H32.png)

![](pepper_H32.png)

![](sudoku_H32.png)


### Vertical stripes - width 32

![](airplane_V32.png)

![](asbest_V32.png)

![](baboon_V32.png)

![](barbara_V32.png)

![](boats_V32.png)

![](BoatsColor_V32.png)

![](goldhill_V32.png)

![](lenna_V32.png)

![](noise_template_V32.png)

![](otsu_V32.png)

![](pepper_V32.png)

![](sudoku_V32.png)
