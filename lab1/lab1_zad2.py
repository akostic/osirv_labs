ulice=[]
max=""
x=0
n=raw_input("Upisi naziv ulice duljine 7 do 15 slova: ")

if (len(n)<7 or len(n)>15) and n!="prekid":
        print "Duljina nije izmedu 7 i 15."
else:
    x+=1
    while n!="prekid":
        n=raw_input("Upisi naziv ulice duljine 7 do 15 slova: ")
        if (len(n)<7 or len(n)>15) and n!="prekid":
            print "Duljina nije izmedu 7 i 15."
        else:
            ulice.append(n)
            if n!="prekid":
                x+=1
            if len(n) > len(max) and n!="prekid":
                max=n
if x==0:
    print "Nije unesena nijedna ulica."
else:
    print "Uneseno je",x,"ulica, a najduze ime ulice je '",max,"'."
